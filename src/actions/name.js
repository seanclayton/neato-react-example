// Constants
export const UPDATE_NAME = 'UPDATE_NAME'

// Actions
export const updateName = name => ({
  type: UPDATE_NAME,
  payload: name
})
