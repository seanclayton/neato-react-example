import React, { PropTypes } from 'react'
import CSSModules from 'react-css-modules'
import styles from './Counter.css'

const Counter = ({ count, increase }) =>
  <button styleName="button" onClick={() => increase()}>
    Count: {count}
  </button>

Counter.propTypes = {
  count: PropTypes.number.isRequired,
  increase: PropTypes.func.isRequired
}

export default CSSModules(Counter, styles)
