import React from 'react'
import CounterContainer from 'containers/CounterContainer'

const CounterView = () =>
  <CounterContainer />

export default CounterView
