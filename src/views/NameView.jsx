import React from 'react'
import NameChangerContainer from 'containers/NameChangerContainer'

const NameView = () =>
  <NameChangerContainer />

export default NameView
