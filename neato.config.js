module.exports = {
  pages: ['index'],
  vendor: [
    'react',
    'react-redux',
    'redux',
    'react-css-modules',
    'react-router',
    'react-router-redux',
    'react-dom',
    'lodash',
    'redux-saga',
    'redux-thunk'
  ]
}
