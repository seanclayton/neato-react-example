# Neato Example

### Things to Notice
- No webpack configuration _required_
- Zero boilerplate (for setting up a project 😉)
- Completely Reconfigurable

### Benefits
- _Just start coding!_ With other boilerplates/starter kits, you have to a bunch of work at the beginning of a project.
- No webpack! Well, sort of. Webpack is underneath Neato, but we manage it for you! No wiring on your part!

### Cons
- It's magic. That is, until you read the [source code](https://gitlab.com/seanclayton/neato) 😛
- It may have some [issues](https://gitlab.com/seanclayton/neato/issues)
